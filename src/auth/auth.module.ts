import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { BearerStrategy } from './strategy';

@Module({
  controllers: [AuthController],
  providers: [AuthService, JwtService, BearerStrategy],
})
export class AuthModule {}
