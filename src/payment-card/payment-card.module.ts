import { Module } from '@nestjs/common';
import { PaymentCardService } from './payment-card.service';
import { PaymentCardController } from './payment-card.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { PaymentCard, PaymentCardSchema } from './schema/payment-card.schema';
import { AuthService } from 'src/auth/auth.service';
import { Luhn } from './validator';
import { BearerStrategy } from 'src/auth/strategy';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: PaymentCard.name,
        schema: PaymentCardSchema,
      },
    ]),
    JwtModule.register({}),
  ],
  controllers: [PaymentCardController],
  providers: [PaymentCardService, AuthService, BearerStrategy, Luhn],
})
export class PaymentCardModule {}
