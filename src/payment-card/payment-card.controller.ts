import {
  Controller,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { PaymentCardService } from './payment-card.service';
import { CreatePaymentCardDto, UpdatePaymentCardDto } from './dto';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('bearer'))
@Controller('tokens')
export class PaymentCardController {
  constructor(private readonly paymentCardService: PaymentCardService) {}

  @Post()
  create(@Body() createTokenDto: CreatePaymentCardDto) {
    console.log('createTokenDto', createTokenDto);
    return this.paymentCardService.create(createTokenDto);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTokenDto: UpdatePaymentCardDto,
  ) {
    return this.paymentCardService.update(+id, updateTokenDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentCardService.remove(+id);
  }
}
