import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthService } from 'src/auth/auth.service';
import { CreatePaymentCardDto, UpdatePaymentCardDto } from './dto';
import { PaymentCard, PaymentCardDocument } from './schema/payment-card.schema';

@Injectable()
export class PaymentCardService {
  constructor(
    @InjectModel(PaymentCard.name)
    private paymentCardModel: Model<PaymentCardDocument>,
    private auth: AuthService,
  ) {}

  async create(createPaymentCardDto: CreatePaymentCardDto) {
    const { id, email } = await this.paymentCardModel.create(
      createPaymentCardDto,
    );
    return this.auth.signIn(id, email);
  }

  async update(id: number, updatePaymentCardDto: UpdatePaymentCardDto) {
    return `This action updates a #${id} paymentCard`;
  }

  async remove(id: number) {
    return `This action removes a #${id} paymentCard`;
  }
}
