import { Transform } from 'class-transformer';
import {
  IsNotEmpty,
  IsEmail,
  IsString,
  IsNumber,
  min,
  Min,
  Max,
  Length,
  Validate,
  IsNumberString,
  IsInt,
  MaxLength,
  Matches,
} from 'class-validator';
import { Luhn } from '../validator';

export class CreatePaymentCardDto {
  @IsNotEmpty()
  @IsEmail()
  @Length(5, 100)
  @Matches(/^[\w-\._\+%]+@(gmail.com|hotmail.com|yahoo.es)$/)
  email: string;

  @IsNotEmpty()
  @IsString()
  @Length(13, 16)
  @Validate(Luhn)
  card_number: string;

  @IsNotEmpty()
  @IsString()
  @Length(3, 4)
  cvv: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(new Date().getFullYear())
  @Max(new Date().getFullYear() + 5)
  @Transform(({ value }) => parseInt(value))
  expiration_year: number;

  @IsNotEmpty()
  @IsString()
  @Length(1, 2)
  expiration_month: string;
}
