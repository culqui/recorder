import { Injectable } from '@nestjs/common';
import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import * as luhn from 'luhn';
import { DEFAULT_MESSAGE } from './static.validator';

@ValidatorConstraint({ name: 'Luhn', async: true })
@Injectable()
export class Luhn implements ValidatorConstraintInterface {
  async validate(value: string) {
    try {
      // await this.usersRepository.getOneOrFail(value);
      const isValid = luhn.validate(value);
      if (!isValid) throw DEFAULT_MESSAGE;
    } catch (e) {
      return false;
    }

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    console.log('args', args);
    return DEFAULT_MESSAGE;
  }
}
